export class apiUrls {
    public static FLICKR_API_URL = 'https://api.flickr.com/services/rest/';
    public static GET_METHOD_INTERESTINGS = 'flickr.interestingness.getList';
    public static GET_METHOD_PEOPLE_INFO = 'flickr.people.getInfo';
    public static GET_METHOD_PHOTOS_INFO = 'flickr.photos.getInfo';
    public static GET_METHOD_PHOTOS_SEARCH = 'flickr.photos.search';
    public static GET_METHOD_ALL_CONTEXTS = 'flickr.photos.getAllContexts';
    public static GET_METHOD_PHOTOSET_CONTEXT = "flickr.photosets.getContext";
    public static PER_PAGE = '10';
}

