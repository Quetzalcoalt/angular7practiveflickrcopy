import { Component, OnInit } from '@angular/core';
import { ImagesService } from 'src/app/Services/images.service';
import { ActivatedRoute } from '@angular/router';
import { ImageDetails } from 'src/app/Models/imageDetailsModel';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-image-details',
  templateUrl: './image-details.component.html',
  styleUrls: ['./image-details.component.scss']
})
export class ImageDetailsComponent implements OnInit {

    imageId: string;
    prevPhotoId: string;
    nextPhotoId: string;
    image: ImageDetails;
    imageBlobUrl;
    isLoading: boolean;
    loadPercentage: number;
    getBlobSubsciber;

    constructor(private imageService: ImagesService, private route: ActivatedRoute,) { }

    ngOnInit(){
        this.imageService.currentPercentage.subscribe(percentage => this.loadPercentage = percentage)
        this.route.params.subscribe(params => {
            this.isLoading = true;
            this.imageId = params.id;
            this.getNextAndPreviousImages(this.imageId);
        })
    }

    getNextAndPreviousImages(imageId: String): void{
        this.imageService.getImageSetId(imageId).subscribe(data => {
            let setId = data.set[0].id;
            let secret = data.set[0].secret;
            this.imageService.getPhotoSetContext(imageId,setId).subscribe(set => {
                this.prevPhotoId = set.prevphoto.id;
                this.nextPhotoId = set.nextphoto.id;
            })
            this.imageService.getImageDetails(imageId, secret).subscribe(data => {
                this.imageService.getUserData(data.photo.owner.nsid).subscribe(user => { 
                    this.image = new ImageDetails(data.photo, user.person);
                    this.loadImageBeforeBinding(this.image.imageUrl);   
                })
            })
        })
    }

    cancelImageBlobRequest(){
        if(this.getBlobSubsciber !== undefined){
            this.getBlobSubsciber.unsubscribe();
        }
    }

    loadImageBeforeBinding(imgUrl: String){
        this.getBlobSubsciber = this.imageService.getImageBlob(imgUrl).subscribe(data => {
            if(data.body !== undefined){
                this.createImageFromBlob(data.body);
            }
        })
    }

    createImageFromBlob(image: Blob){
        let reader = new FileReader();
        reader.addEventListener("load", () => {
            this.imageBlobUrl = reader.result;
            this.isLoading = false;
        }, false);
        if(image){
            reader.readAsDataURL(image);
        }
    }

}
