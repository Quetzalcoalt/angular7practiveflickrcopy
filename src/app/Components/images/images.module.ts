import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeferLoadModule } from '@trademe/ng-defer-load';

import { ImagesRoutingModule } from './images-routing.module';
import { ImageDetailsComponent } from '../../Components/images/image-details/image-details.component';

//Materials
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


@NgModule({
  declarations: [ImageDetailsComponent],
  imports: [
    CommonModule,
    ImagesRoutingModule,
    DeferLoadModule,

    //Materials
    MatIconModule,
    MatProgressSpinnerModule
  ]
})
export class ImagesModule { }
