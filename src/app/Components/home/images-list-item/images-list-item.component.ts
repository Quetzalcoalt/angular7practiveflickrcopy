import { Component, OnInit, Input } from '@angular/core';
import { Image } from 'src/app/Models/imageModel';

@Component({
  selector: 'app-images-list-item',
  templateUrl: './images-list-item.component.html',
  styleUrls: ['./images-list-item.component.scss']
})
export class ImagesListItemComponent implements OnInit {

    @Input() image: Image;

    constructor() { }

    ngOnInit() {
        
    }

}
