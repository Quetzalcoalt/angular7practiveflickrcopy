import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { ImagesListComponent } from './images-list/images-list.component';
import { ImagesListItemComponent } from './images-list-item/images-list-item.component';

//Materials
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from  '@angular/material/form-field'
import { MatButtonModule } from  '@angular/material/button'
import { MatInputModule } from  '@angular/material/input'
import { MatIconModule } from  '@angular/material/icon'
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
    declarations: [
        HomeComponent,
        SearchComponent,
        ImagesListComponent,
        ImagesListItemComponent
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,

        //Materials
        FlexLayoutModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatDividerModule
    ]
})
export class HomeModule { }
