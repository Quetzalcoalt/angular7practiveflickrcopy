import { Component, OnInit } from '@angular/core';
import { Image } from 'src/app/Models/imageModel';
import { ImagesService } from 'src/app/Services/images.service';

@Component({
  selector: 'app-images-list',
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.scss']
})
export class ImagesListComponent implements OnInit {

    images: Image[] = [];

    constructor(private imageService: ImagesService) { }

    ngOnInit() {
        this.getImages();
    }

    getImages(): void {
        this.imageService.getImages().subscribe(data => {
            // console.log(data.photos);
            data.photos.photo.forEach(image => {
                this.imageService.getUserData(image.owner).subscribe(user => {
                    this.images.push(new Image(
                        image, 
                        user.person
                    ))
                })
            });
        });
    }
}
