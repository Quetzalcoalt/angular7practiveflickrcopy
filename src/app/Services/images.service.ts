import { Injectable, Output } from '@angular/core';
import { Image } from '../Models/imageModel';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpEvent, HttpEventType } from '@angular/common/http';
import { catchError, map, tap, last } from 'rxjs/operators';
import { KEYS } from '../../apiKeys'
import { apiUrls } from '../../ApiUrls'


@Injectable({
  providedIn: 'root'
})
export class ImagesService {

    private downloadPercentage = new BehaviorSubject<number>(0);
    currentPercentage = this.downloadPercentage.asObservable();

    constructor(private http: HttpClient) { }

    getImages(): Observable<any>{
        const options = {
            params: new HttpParams()
            .set('method', apiUrls.GET_METHOD_INTERESTINGS)
            .set('api_key', KEYS.KEY)
            .set('per_page', apiUrls.PER_PAGE)
            .set('page', '1')
            .set('format','json')
            .set('nojsoncallback', '1')
        }
        return this.http.get(apiUrls.FLICKR_API_URL, options).pipe(
            catchError(this.handleError('getImages', []))
        );
    }

    getImageBlob(imgUrl): Observable<any>{
        const req = new HttpRequest("GET", imgUrl, {
            reportProgress: true,
            responseType: 'blob' as 'json'
        })
        return this.http.request(req).pipe(
            map(event => this.getPercentageDone(event)),
            tap(percentage => this.showProgress(percentage)),
            catchError(this.handleError("getImageBlob", []),)
        )
    }

    private showProgress(percentage){
        // console.log(percentage)
        this.downloadPercentage.next(percentage);
    }

    private getPercentageDone(event: HttpEvent<any>) {
        switch (event.type) {     
            case HttpEventType.DownloadProgress:
                // Compute and show the % done:
                const percentDone = Math.round(100 * event.loaded / event.total);
                return percentDone;
            default:
                return event;
        }
    }

    getUserData(userId): Observable<any>{
        const options = {
            params: new HttpParams()
            .set('method', apiUrls.GET_METHOD_PEOPLE_INFO)
            .set('api_key', KEYS.KEY)
            .set('user_id', userId)
            .set('format','json')
            .set('nojsoncallback', '1')
        }
        return this.http.get(apiUrls.FLICKR_API_URL, options).pipe(
            catchError(this.handleError('getUserData', []))
        );
    }

    getImageSetId(imageId): Observable<any>{
        const options = {
            params: new HttpParams()
            .set('method', apiUrls.GET_METHOD_ALL_CONTEXTS)
            .set('api_key', KEYS.KEY)
            .set('photo_id', imageId)
            .set('format','json')
            .set('nojsoncallback', '1')
        }
        return this.http.get(apiUrls.FLICKR_API_URL, options).pipe(
            catchError(this.handleError('getImageSetId', []))
        );
    }

    getPhotoSetContext(photoId, setId): Observable<any>{
        const options = {
            params: new HttpParams()
            .set('method', apiUrls.GET_METHOD_PHOTOSET_CONTEXT)
            .set('api_key', KEYS.KEY)
            .set('photo_id', photoId)
            .set('photoset_id', setId)
            .set('format','json')
            .set('nojsoncallback', '1')
        }
        return this.http.get(apiUrls.FLICKR_API_URL, options).pipe(
            catchError(this.handleError('getPhotoSetContext', []))
        );
    }

    getImageDetails(imageId, secret): Observable<any>{
        const options = {
            params: new HttpParams()
            .set('method', apiUrls.GET_METHOD_PHOTOS_INFO)
            .set('api_key', KEYS.KEY)
            .set('photo_id', imageId)
            .set('secret', secret)
            .set('format','json')
            .set('nojsoncallback', '1')
        }
        return this.http.get(apiUrls.FLICKR_API_URL, options).pipe(
            catchError(this.handleError('getImageDetails', []))
        );
    }

    private handleError<T> (operation = 'operation', result?: T){
        return (error : any): Observable<T> => {
            console.error(error);
            console.log(`${operation} failed: ${error.message}`);
            return of(result as T);
        }
    }
}
