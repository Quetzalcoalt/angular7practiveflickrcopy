import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Components
import { AboutComponent } from './Views/about/about.component';

const routes: Routes = [
    { path: '', loadChildren: './Components/home/home.module#HomeModule' },
    { path: 'about', component: AboutComponent },
    { path: 'image', loadChildren: './Components/images/images.module#ImagesModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
