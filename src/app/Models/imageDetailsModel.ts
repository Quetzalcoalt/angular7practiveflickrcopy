export class ImageDetails {
    userAvatarUrl: string;
    imageId: string;
    userId: string;
    title: string;
    username: string;
    imageUrl: string; 

    constructor(image, user){
        let username;
        if(user.username._content !== undefined){
            username = user.username._content
        }else{
            username = user.realname._content
        }

        this.userId = image.owner.nsid;
        this.imageId = image.id;
        this.title = image.title._content;
        this.imageUrl = "https://farm" + image.farm + ".staticflickr.com/" + image.server + "/" + image.id + "_" + image.secret + "_h.jpg"
        this.username = username;
        this.userAvatarUrl = "http://farm" + user.iconfarm + ".staticflickr.com/" + user.iconserver + "/buddyicons/" + user.id + ".jpg";
    }
}