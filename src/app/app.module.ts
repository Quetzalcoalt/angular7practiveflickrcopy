import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

//Routing
import { AppRoutingModule } from './app-routing.module';

//Components
import { AppComponent } from './app.component';
import { NavigationComponent } from './Components/navigation/navigation.component';
import { AboutComponent } from './Views/about/about.component';

//Moudles 
// import { HomeModule } from './Components/home/home.module'
// import { ImagesModule } from './Components/images/images.module'
// import { UserModule } from './Components/user/user.module'

//Material section
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './app.material.module';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AboutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppMaterialModule, 
    // HomeModule,
    // ImagesModule,
    // UserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
